#include <stdlib.h>
#include <math.h>
#include <stddef.h>
#include <SDL2/SDL.h>
#include "globaldefs.h"
#include "textures.h"
#include "graphics.h"
#include "input.h"
#include "deltatime.h"
#include "actors/actor.h"
#include <time.h>
#include "sound.h"
#include "font.h"

int main(int argc, char** argv)
{
    if(InitGraphics()!=0)
    {
        return 0;
    }
    srand(time(NULL));

    int isRunning = 1;
    DeltaTimeInit();
    keys.up = 0;
    keys.down = 0;
    keys.left = 0;
    keys.right = 0;

    for(int i=0;i<10;i++)
    {
        CreateActor(ACTOR_VIDYABUTTS);
    }
    for(int i=0;i<15;i++)
    {
        CreateActor(ACTOR_GOOBUS);
    }
    LoadTexture(TEX_MIGO);

    struct DrawCall migodc;
    migodc.texturenumber = TEX_MIGO;
    migodc.x = SCREENWIDTH/2.0f;
    migodc.y = SCREENHEIGHT/2.0f;
    migodc.z = 0.0f;
    migodc.filter = GL_LINEAR;
    migodc.w = (float)SCREENWIDTH;
    migodc.h = (float)SCREENHEIGHT;
    migodc.theta = 0.0f;
    migodc.r = 1.0f;
    migodc.g = 1.0f;
    migodc.b = 1.0f;
    migodc.a = 1.0f;
    migodc.tc.x = 0.0f;
    migodc.tc.y = 0.0f;
    migodc.tc.w = 1.0f;
    migodc.tc.h = 1.0f;
    migodc.temptex = 0;

    LoadFont(FONT_NOTO24);
    PlayMusic(MUS_AYAYA);
    while(isRunning)
    {
        int eventCode = HandleEvents();
        if(eventCode == 1)
            isRunning = 0;

        ClearRender(0.0f,0.5f,0.5f);
        StartRender();

        DrawTex(&migodc);

        ProcessActors();

        char* timeelapsed = malloc(1024);

        sprintf(timeelapsed,"Time elapsed: %f",deltaTime);

        //printf(keystat);

        struct TextCall tc;
        tc.text = timeelapsed;
        tc.x = 23, tc.y = 23;
        tc.w = 32;
        tc.scale = 4.0f;
        tc.r = 0.0f, tc.g = 0.0f, tc.b = 0.0f, tc.a = 1.0f;
        tc.freetext = 0;
        tc.z = 200.0f;
        tc.xalign = 0;
        tc.yalign = 0;

        DrawText(&tc);

        tc.x = 20, tc.y = 20;
        tc.g = 1.0f;
        tc.z = 201.0f;
        tc.freetext = 1;

        DrawText(&tc);

        char ttfass[] = "my mom asshoe";

        struct TTFCall ttc;
        ttc.text = ttfass;
        ttc.x = SCREENWIDTH/2;
        ttc.y = 500;
        ttc.r = 0.0f, ttc.g = 0.0f, ttc.b = 1.0f, ttc.a = 1.0f;
        ttc.freetext = 0;
        ttc.font = FONT_NOTO24;
        ttc.z = 202.0f;
        ttc.scale = 1.5f;
        ttc.xalign = 1;
        ttc.yalign = 1;

        DrawTTF(&ttc);

        PresentRender();

        DeltaTimeUpdate();
    }
    return 0;
}