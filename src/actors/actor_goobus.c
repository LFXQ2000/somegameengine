#include "actor.h"
#include "actor_goobus.h"
#include "graphics.h"
#include "textures.h"
#include "deltatime.h"
#include "globaldefs.h"
#include <math.h>

#define VARS ActorList[actorID].vars
#define SCALE FVAR[0]
#define XSPEED ActorList[actorID].xspeed
#define YSPEED ActorList[actorID].yspeed
#define XPOS ActorList[actorID].x
#define YPOS ActorList[actorID].y
#define ANGLE FVAR[1]
#define DANGLE FVAR[2]


void Init_Goobus(int actorID)
{
    VARS = malloc(sizeof(float)*3);
    float* FVAR = (float*)VARS;
    SCALE = ((float)rand()/RAND_MAX)*0.5f+0.05f;
    LoadTexture(TEX_GOOBUS);
    SDL_Point texsize = GetTextureSize(TEX_GOOBUS);
    texsize.x *= SCALE;
    texsize.y *= SCALE;
    XPOS = ((float)rand()/RAND_MAX)*(SCREENWIDTH-texsize.x)+(texsize.x/2.0);
    YPOS = ((float)rand()/RAND_MAX)*(SCREENHEIGHT-texsize.y)+(texsize.y/2.0);
    
    const float speed = ((float)rand()/RAND_MAX)*1500.0f+500.0f;

    const float angle = ((float)rand()/RAND_MAX)*(PI*2);

    ANGLE = ((float)rand()/RAND_MAX)*(PI*2);

    XSPEED = sinf(angle)*speed;
    YSPEED = cosf(angle)*speed;

    DANGLE = ((float)rand()/RAND_MAX)*(PI*2)*((rand()%2)*2-1);

    Main_Goobus(actorID);
}

void Main_Goobus(int actorID)
{
    float* FVAR = (float*)VARS;
    UpdatePos(actorID);

    SDL_Point texsize = GetTextureSize(TEX_GOOBUS);
    texsize.x *= SCALE;
    texsize.y *= SCALE;

    if((XPOS < texsize.x/2.0 && XSPEED < 0) || (XPOS > SCREENWIDTH-texsize.x/2.0 && XSPEED > 0))
        XSPEED *= -1;

    if((YPOS < texsize.y/2.0 && YSPEED < 0) || (YPOS > SCREENHEIGHT-texsize.y/2.0 && YSPEED > 0))
        YSPEED *= -1;

    ANGLE = fmod(ANGLE+DANGLE*deltaTime,PI*2);

    struct DrawCall goobdc;
    goobdc.texturenumber = TEX_GOOBUS;
    goobdc.x = XPOS;
    goobdc.y = YPOS;
    goobdc.z = 1.0f;
    goobdc.filter = GL_LINEAR;
    goobdc.w = texsize.x;
    goobdc.h = texsize.y;
    goobdc.theta = ANGLE;
    goobdc.r = 1.0f;
    goobdc.g = 1.0f;
    goobdc.b = 1.0f;
    goobdc.a = 1.0f;
    goobdc.tc.x = 0.0f;
    goobdc.tc.y = 0.0f;
    goobdc.tc.w = 1.0f;
    goobdc.tc.h = 1.0f;
    goobdc.temptex = 0;

    DrawTex(&goobdc);
}

void Kill_Goobus(int actorID)
{
    free(VARS);
}