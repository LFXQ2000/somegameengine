#include "actorlist.h"

struct Actor {
    int actortype;
    int status;
    float x,y;
    float xspeed,yspeed;
    unsigned char* hitbox;
    void* vars;
};

extern struct Actor ActorList[];

enum
{
    ACTSTATE_FREE,
    ACTSTATE_INIT,
    ACTSTATE_MAIN,
    ACTSTATE_KILL
};

int FindActorSlot(int i);

int CreateActor(int type);
int CreateActorOffset(int type, int offset);

void UpdatePos(int actorID);

void ProcessActors();