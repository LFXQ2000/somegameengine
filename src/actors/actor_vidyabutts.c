#include "actor.h"
#include "actor_vidyabutts.h"
#include "graphics.h"
#include "textures.h"
#include "input.h"
#include "deltatime.h"
#include "globaldefs.h"
#include <stdlib.h>
#include "sound.h"

#define VARS ActorList[actorID].vars
#define SCALE FVAR[0]
#define SPEED 600.0f
#define BULLSPEED 800.0f
#define XPOS ActorList[actorID].x
#define YPOS ActorList[actorID].y

void Init_VidyaButts(int actorID)
{
    VARS = malloc(sizeof(float)*1);
    float* FVAR = (float*)VARS;
    SCALE = ((float)rand()/RAND_MAX)*0.4f+0.1f;
    LoadTexture(TEX_VIDYABUTTS);
    SDL_Point texsize = GetTextureSize(TEX_VIDYABUTTS);
    texsize.x *= SCALE;
    texsize.y *= SCALE;
    XPOS = ((float)rand()/RAND_MAX)*(SCREENWIDTH-texsize.x)+(texsize.x/2.0);
    YPOS = ((float)rand()/RAND_MAX)*(SCREENHEIGHT-texsize.y)+(texsize.y/2.0);
    Main_VidyaButts(actorID);
}

void Main_VidyaButts(int actorID)
{
    float* FVAR = (float*)VARS;
    if(keys.left & KEY_HELD)
    {
        XPOS -= SPEED*SCALE*deltaTime;
    }
    if(keys.right & KEY_HELD)
    {
        XPOS += SPEED*SCALE*deltaTime;
    }
    if(keys.up & KEY_HELD)
    {
        YPOS -= SPEED*SCALE*deltaTime;
    }
    if(keys.down & KEY_HELD)
    {
        YPOS += SPEED*SCALE*deltaTime;
    }
    if(keys.space & KEY_PRESSED)
    {
        const float bullx = XPOS+(155.0f*SCALE);
        const float bully = YPOS+(-172.5f*SCALE);
        for(int i = 0; i < 8; i++)
        {
            const int bull = CreateActor(ACTOR_BULLET);
            const float theta = (i/4.0f)*PI;
            ActorList[bull].xspeed = sinf(theta)*BULLSPEED;
            ActorList[bull].yspeed = cosf(theta)*BULLSPEED;
            ActorList[bull].x = XPOS+(155.0f*SCALE);
            ActorList[bull].y = YPOS+(-172.5f*SCALE);
        }
        PlaySound(SND_BULLET,1);
    }
    if(mouse.left & KEY_PRESSED)
    {
        const float bullx = XPOS+(155.0f*SCALE);
        const float bully = YPOS+(-172.5f*SCALE);
        
        const int bull = CreateActor(ACTOR_BULLET);
        const float theta = atan2f(mouse.y-bully,mouse.x-bullx);
        ActorList[bull].xspeed = cosf(theta)*BULLSPEED;
        ActorList[bull].yspeed = sinf(theta)*BULLSPEED;
        ActorList[bull].x = bullx;
        ActorList[bull].y = bully;
        PlaySound(SND_BULLET,1);
    }

    SDL_Point texsize = GetTextureSize(TEX_VIDYABUTTS);
    texsize.x *= SCALE;
    texsize.y *= SCALE;

    struct DrawCall buttdc;
    buttdc.texturenumber = TEX_VIDYABUTTS;
    buttdc.x = XPOS;
    buttdc.y = YPOS;
    buttdc.z = 2.0f+SCALE;
    buttdc.filter = GL_LINEAR;
    buttdc.w = texsize.x;
    buttdc.h = texsize.y;
    buttdc.theta = 0.0f;
    buttdc.r = 1.0f;
    buttdc.g = 1.0f;
    buttdc.b = 1.0f;
    buttdc.a = 1.0f;
    buttdc.tc.x = 0.0f;
    buttdc.tc.y = 0.0f;
    buttdc.tc.w = 1.0f;
    buttdc.tc.h = 1.0f;
    buttdc.temptex = 0;

    DrawTex(&buttdc);
}

void Kill_VidyaButts(int actorID)
{
    free(VARS);
}