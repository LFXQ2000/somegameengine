#include "actor.h"
#include "globaldefs.h"
#include "deltatime.h"
#include "actor_vidyabutts.h"
#include "actor_goobus.h"
#include "actor_bullet.h"
#include <stddef.h>

struct Actor ActorList[MAXACTORS];

typedef void (*ActFunc)(int);

ActFunc initActFuncs[] =
{
    &Init_VidyaButts,
    &Init_Goobus,
    &Init_Bullet
};

ActFunc mainActFuncs[] =
{
    &Main_VidyaButts,
    &Main_Goobus,
    &Main_Bullet
};

ActFunc killActFuncs[] =
{
    &Kill_VidyaButts,
    &Kill_Goobus,
    &Kill_Bullet
};

int FindActorSlot(int i)
{
    int found = 0;
    while(i < MAXACTORS && found == 0)
    {
        if(ActorList[i].status == ACTSTATE_FREE)
            found = 1;
        else
            i++;
    }
    return found == 1 ? i : -1;
}

int CreateActor(int type)
{
    return CreateActorOffset(type,0);
}

int CreateActorOffset(int type, int offset)
{
    int slot = FindActorSlot(offset);
    if(slot<0)
    {
        return -1;
    }
    struct Actor newactor;
    newactor.status = ACTSTATE_INIT;
    newactor.actortype = type;
    ActorList[slot] = newactor;
    return slot;
}

void UpdatePos(int actorID)
{
    ActorList[actorID].x += ActorList[actorID].xspeed*deltaTime;
    ActorList[actorID].y += ActorList[actorID].yspeed*deltaTime;
}

void ProcessActors()
{
    for(int i=0;i<MAXACTORS;i++)
    {
        switch(ActorList[i].status)
        {
            case ACTSTATE_INIT:
                initActFuncs[ActorList[i].actortype](i);
                if(ActorList[i].status!=ACTSTATE_INIT)
                    break;
                else
                    ActorList[i].status++;
                    break;
            case ACTSTATE_MAIN:
                mainActFuncs[ActorList[i].actortype](i);
                break;
            case ACTSTATE_KILL:
                killActFuncs[ActorList[i].actortype](i);
                ActorList[i].status = ACTSTATE_FREE;
                break;
            default:
                break;
        }
    }
}

void KillActors()
{
    for(int i=0;i<MAXACTORS;i++)
    {
        switch(ActorList[i].status)
        {
            case ACTSTATE_INIT:
                initActFuncs[ActorList[i].actortype](i);
                if(ActorList[i].status==ACTSTATE_FREE)
                    break;
                else
                    ActorList[i].status++;
                    break;
            case ACTSTATE_MAIN:
                mainActFuncs[ActorList[i].actortype](i);
                break;
            case ACTSTATE_KILL:
                killActFuncs[ActorList[i].actortype](i);
                ActorList[i].status = ACTSTATE_FREE;
                break;
            default:
                break;
        }
    }
}