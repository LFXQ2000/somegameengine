#include "actor.h"
#include "actor_bullet.h"
#include "graphics.h"
#include "textures.h"
#include "deltatime.h"
#include "globaldefs.h"
#include <math.h>

#define VARS ActorList[actorID].vars
#define FVARSIZE 1
#define ANIMTIME FVAR[0]
#define FRAME IVAR[0]
#define XSPEED ActorList[actorID].xspeed
#define YSPEED ActorList[actorID].yspeed
#define XPOS ActorList[actorID].x
#define YPOS ActorList[actorID].y

#define FRAMELENGTH 0.133333f
#define SIZE 4.0f

const int frames[] = {
    TEX_BULLET1,
    TEX_BULLET2,
    TEX_BULLET3,
    TEX_BULLET2
};

void Init_Bullet(int actorID)
{
    VARS = malloc(sizeof(float)*FVARSIZE+sizeof(int));
    float* FVAR = (float*)VARS;
    int* IVAR = (int*)(FVAR+FVARSIZE);
    FRAME = 0;
    ANIMTIME = 0.0f;
    LoadTexture(TEX_BULLET1);
    LoadTexture(TEX_BULLET2);
    LoadTexture(TEX_BULLET3);
    Main_Bullet(actorID);
}

void Main_Bullet(int actorID)
{
    float* FVAR = (float*)VARS;
    int* IVAR = (int*)(FVAR+FVARSIZE);
    UpdatePos(actorID);

    SDL_Point texsize = GetTextureSize(TEX_BULLET1);
    texsize.x*=SIZE;
    texsize.y*=SIZE;

    if(XPOS < 0 - texsize.x || XPOS > SCREENWIDTH + texsize.x || YPOS < 0 - texsize.y || YPOS > SCREENHEIGHT + texsize.y)
        ActorList[actorID].status = ACTSTATE_KILL;

    ANIMTIME += deltaTime;
    if(ANIMTIME > FRAMELENGTH)
    {
        ANIMTIME -= FRAMELENGTH;
        FRAME = (FRAME + 1)&3;
    }

    //printf("bull %i frame %i state %i\n",actorID,FRAME,ActorList[actorID].status);

    struct DrawCall bulldc;
    bulldc.texturenumber = frames[FRAME];
    bulldc.x = XPOS;
    bulldc.y = YPOS;
    bulldc.z = 100.0f;
    bulldc.filter = GL_NEAREST;
    bulldc.w = texsize.x;
    bulldc.h = texsize.y;
    bulldc.theta = 0.0f;
    bulldc.r = 1.0f;
    bulldc.g = 1.0f;
    bulldc.b = 1.0f;
    bulldc.a = 1.0f;
    bulldc.tc.x = 0.0f;
    bulldc.tc.y = 0.0f;
    bulldc.tc.w = 1.0f;
    bulldc.tc.h = 1.0f;
    bulldc.temptex = 0;

    DrawTex(&bulldc);
}

void Kill_Bullet(int actorID)
{
    free(VARS);
}