#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <GL/glu.h>
#include <string.h>

extern SDL_Window* window;
//extern SDL_Renderer* renderer;

extern unsigned int loadedTextures[];

#if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
	#define PIXEL_MASK_Red   0xff000000
	#define PIXEL_MASK_Green 0x00ff0000
	#define PIXEL_MASK_Blue  0x0000ff00
	#define PIXEL_MASK_Alpha 0x000000ff
#else
	#define PIXEL_MASK_Red   0x000000ff
	#define PIXEL_MASK_Green 0x0000ff00
	#define PIXEL_MASK_Blue  0x00ff0000
	#define PIXEL_MASK_Alpha 0xff000000
#endif

#define COLOR_32_BIT 32

struct DrawCall
{
    float x,y,z,w,h,theta,r,g,b,a;
    SDL_Rect tc;
    unsigned int texturenumber;
    GLint filter;
    int temptex;
};

struct TextCall
{
    int x,y,w;
    float z;
    float scale;
    char* text;
    float r,g,b,a;
    int freetext;
    int xalign,yalign;
};

struct TTFCall
{
    int x,y;
    float z;
    float scale;
    char* text;
    float r,g,b,a;
    int font;
    int freetext;
    int xalign,yalign;
};

void LoadTexture(unsigned int texturenumber);

SDL_Point GetTextureSize(unsigned int texturenumber);

int InitGraphics();

void DrawTex(struct DrawCall* dc);

void DrawText(struct TextCall* tc);

void DrawTTF(struct TTFCall* ttc);

void ClearRender(float r,float g,float b);
void StartRender();
void PresentRender();