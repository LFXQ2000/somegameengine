#include <SDL2/SDL.h>
#include "sound.h"

Mix_Chunk* loadedSounds[SND_NUM];
Mix_Music* loadedMusic[MUS_NUM];

void InitSounds()
{
    for(int i=1;i<SND_NUM;i++)
    {
        SDL_RWops* rwops = SDL_RWFromConstMem((void*)raw_sound_list[i].data,raw_sound_list[i].size);
        loadedSounds[i] = Mix_LoadWAV_RW(rwops,1);
    }
    for(int i=1;i<MUS_NUM;i++)
    {
        SDL_RWops* rwops = SDL_RWFromConstMem((void*)raw_music_list[i].data,raw_music_list[i].size);
        loadedMusic[i] = Mix_LoadMUS_RW(rwops,1);
    }
}

void PlaySound(int soundNum, int channel)
{
    Mix_PlayChannel(channel,loadedSounds[soundNum],0);
}

void PlayMusic(int musicNum)
{
    Mix_PlayMusic(loadedMusic[musicNum],-1);
}