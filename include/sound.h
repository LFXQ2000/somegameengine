#include <SDL2/SDL_mixer.h>
#include "soundlist.h"

void InitSounds();
void PlaySound(int soundNum, int channel);
void PlayMusic(int musicNum);