#include <SDL2/SDL.h>

enum
{
    KEY_RELEASED = 0,
    KEY_HELD = 1,
    KEY_PRESSED = 2
};

struct Keys
{
    int left;
    int right;
    int up;
    int down;
    int space;
};

struct Mouse
{
    int x,y;
    int left,middle,right;
};

extern struct Keys keys;
extern struct Mouse mouse;

int HandleEvents();