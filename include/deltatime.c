#include "deltatime.h"
#include "SDL2/SDL.h"

float deltaTime = 0.02;
uint64_t LAST = 0;
uint64_t NOW = 1;

void DeltaTimeInit()
{
    NOW = SDL_GetPerformanceCounter();
}

void DeltaTimeUpdate()
{
    LAST = NOW;
    NOW = SDL_GetPerformanceCounter();
    deltaTime = (float)((NOW-LAST)/(float)SDL_GetPerformanceFrequency());
}