#include <stddef.h>
#define SCREENWIDTH 1280
#define SCREENHEIGHT 720
#define WINDOWTITLE "Penor"
#define MAXACTORS 4096
#define PI 3.141592653589793

#ifndef NAMFILE
#define NAMFILE

struct NamFile {
    unsigned char* data;
    size_t size;
};
#endif



#define DEBUGMSGBOX(msg) SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION,msg,msg,window)