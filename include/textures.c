//Generated by texgen.py

#include "textures.h"

const struct NamTex texture_list[] = {
{0,0,NULL},
{16,16,tex_bullet1_data},
{16,16,tex_bullet2_data},
{16,16,tex_bullet3_data},
{8,1024,tex_font_data},
{436,437,tex_goobus_data},
{1103,621,tex_migo_data},
{930,645,tex_vidyabutts_data},
};

