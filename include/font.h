#include <SDL2/SDL_ttf.h>
#include "ttflist.h"

enum
{
    FONT_NOTO24,
    FONT_SIZE
};

struct NamFont
{
    int ttf;
    int size;
    TTF_Font* font;
};

extern struct NamFont namfonts[];

void LoadFont(unsigned int fontnum);