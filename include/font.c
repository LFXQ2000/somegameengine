#include <SDL2/SDL.h>
#include "font.h"

struct NamFont namfonts[] =
{
    {TTF_NOTOSANSREG,72,NULL},
};

void LoadFont(unsigned int fontnum)
{
    if(namfonts[fontnum].font!=NULL || fontnum >= FONT_SIZE)
    {
        return;
    }
    SDL_RWops* fontrw = SDL_RWFromConstMem((void*)raw_ttf_list[fontnum].data,raw_ttf_list[fontnum].size);
    namfonts[fontnum].font = TTF_OpenFontRW(fontrw,1,namfonts[fontnum].size);
}