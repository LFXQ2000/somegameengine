#include "input.h"

struct Keys keys = {

};

struct Mouse mouse = {

};

void HandleKeyPress(int* key)
{
    if(*key & (KEY_PRESSED|KEY_HELD))
        *key = KEY_HELD;
}

int HandleEvents()
{
    SDL_Event e;
    HandleKeyPress(&keys.left);
    HandleKeyPress(&keys.right);
    HandleKeyPress(&keys.up);
    HandleKeyPress(&keys.down);
    HandleKeyPress(&keys.space);

    HandleKeyPress(&mouse.left);
    HandleKeyPress(&mouse.middle);
    HandleKeyPress(&mouse.right);
    int retcode = 0;
    while(SDL_PollEvent(&e)!=0)
    {
        switch(e.type)
        {
            case SDL_QUIT:
                retcode = 1;
                break;
            case SDL_KEYDOWN:
                if(e.key.repeat == 0)
                {
                    switch(e.key.keysym.sym)
                    {
                        case SDLK_LEFT:
                            keys.left = KEY_PRESSED|KEY_HELD;
                            break;
                        case SDLK_RIGHT:
                            keys.right = KEY_PRESSED|KEY_HELD;
                            break;
                        case SDLK_UP:
                            keys.up = KEY_PRESSED|KEY_HELD;
                            break;
                        case SDLK_DOWN:
                            keys.down = KEY_PRESSED|KEY_HELD;
                            break;
                        case SDLK_SPACE:
                            keys.space = KEY_PRESSED|KEY_HELD;
                            break;
                    }
                }
                break;
            case SDL_KEYUP:
                switch(e.key.keysym.sym)
                {
                    case SDLK_LEFT:
                        keys.left = KEY_RELEASED;
                        break;
                    case SDLK_RIGHT:
                        keys.right = KEY_RELEASED;
                        break;
                    case SDLK_UP:
                        keys.up = KEY_RELEASED;
                        break;
                    case SDLK_DOWN:
                        keys.down = KEY_RELEASED;
                        break;
                    case SDLK_SPACE:
                        keys.space = KEY_RELEASED;
                        break;
                }
                break;
            case SDL_MOUSEMOTION:
                SDL_GetMouseState(&mouse.x,&mouse.y);
                break;
            case SDL_MOUSEBUTTONDOWN:
                SDL_GetMouseState(&mouse.x,&mouse.y);
                switch(e.button.button)
                {
                    case SDL_BUTTON_LEFT:
                        mouse.left = KEY_PRESSED|KEY_HELD;
                        break;
                    case SDL_BUTTON_MIDDLE:
                        mouse.middle = KEY_PRESSED|KEY_HELD;
                        break;
                    case SDL_BUTTON_RIGHT:
                        mouse.right = KEY_PRESSED|KEY_HELD;
                        break;
                }
                break;
            case SDL_MOUSEBUTTONUP:
                SDL_GetMouseState(&mouse.x,&mouse.y);
                switch(e.button.button)
                {
                    case SDL_BUTTON_LEFT:
                        mouse.left = KEY_RELEASED;
                        break;
                    case SDL_BUTTON_MIDDLE:
                        mouse.middle = KEY_RELEASED;
                        break;
                    case SDL_BUTTON_RIGHT:
                        mouse.right = KEY_RELEASED;
                        break;
                }
                break;
        }
    }
    return retcode;
}