#include "graphics.h"
#include "textures.h"
#include "globaldefs.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "sound.h"
#include "font.h"
#include "input.h"

#define MAXCALLS 4096
#define MAXTEXT 256
#define MAXTTF 256

SDL_Window* window;
SDL_GLContext windowcontext;
//SDL_Renderer* renderer;

unsigned int loadedTextures[TEX_NUM];
int texLoaded[TEX_NUM];

unsigned int callnum = 0,textnum = 0,ttfnum = 0;

struct DrawCall drawcalls[MAXCALLS];

struct TextCall textcalls[MAXTEXT];

struct TTFCall ttfcalls[MAXTTF];

void LoadTexture(unsigned int texturenumber)
{
    if(texLoaded[texturenumber]!=0)
        return;
    
    glGenTextures(1,&loadedTextures[texturenumber]);
    glBindTexture(GL_TEXTURE_2D,loadedTextures[texturenumber]);
    
    struct NamTex tex = texture_list[texturenumber];
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,tex.width,tex.height,0,GL_RGBA,GL_UNSIGNED_BYTE,tex.data);
    //glGenerateMipmap(GL_TEXTURE_2D);
    texLoaded[texturenumber] = 1;
}

SDL_Point GetTextureSize(unsigned int texturenumber)
{
    if(texLoaded[texturenumber]==0)
        return (SDL_Point){0,0};
    SDL_Point size;
    size.x = texture_list[texturenumber].width;
    size.y = texture_list[texturenumber].height;
    return size;
}

void DrawTex(struct DrawCall* dc)
{
    if(callnum>=MAXCALLS)
    {
        return;
    }
    drawcalls[callnum] = *dc;

    callnum++;
    //printf("%f, %f, %f, %f - %i, %i, %i, %i\r",xnorm,ynorm,wnorm,hnorm,x,y,w,h);

}

void DrawText(struct TextCall* tc)
{
    if(textnum>=MAXTEXT)
    {
        return;
    }
    textcalls[textnum] = *tc;
    textnum++;
}

void DrawTTF(struct TTFCall* ttc)
{
    if(ttfnum>=MAXTTF)
    {
        return;
    }
    ttfcalls[ttfnum] = *ttc;
    ttfnum++;
}










int InitGL()
{

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,24);
    
    windowcontext = SDL_GL_CreateContext(window);
    if(windowcontext == NULL)
    {
        printf("Could not initialize OpenGL context: %s\n",SDL_GetError());
        return 1;
    }
    if(SDL_GL_SetSwapInterval(1)<0)
    {
        printf("Could not initialize OpenGL vsync: %s\n",SDL_GetError());
        return 1;
    }

    int success = 1;
    GLenum error = GL_NO_ERROR;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,SCREENWIDTH,SCREENHEIGHT,0,0.0,1.0);

    error = glGetError();
    if(error != GL_NO_ERROR)
    {
        printf("Could not initialize OpenGL projection matrix: %s\n",gluErrorString(error));
        return 1;
    }
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    error = glGetError();
    if(error != GL_NO_ERROR)
    {
        printf("Could not initialize OpenGL model matrix: %s\n",gluErrorString(error));
        return 1;
    }

    glEnable(GL_TEXTURE_2D);
    if(error != GL_NO_ERROR)
    {
        printf("Could not initialize OpenGL textures: %s\n",gluErrorString(error));
        return 1;
    }
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);

    glEnable(GL_COLOR_MATERIAL);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

    return 0;
}

int InitGraphics()
{
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO)<0)
    {
        printf("Could not initialize SDL: %s\n",SDL_GetError());
        return 1;
    }
    else
    {
        if(Mix_OpenAudio(44100,MIX_DEFAULT_FORMAT,2,2048)<0)
        {
            printf("Could not initialize mixer: %s\n",SDL_GetError());
            return 1;
        }
        if(TTF_Init()<0)
        {
            printf("Could not initialize TTF: %s\n",SDL_GetError());
            return 1;
        }
        InitSounds();
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,1);
        window = SDL_CreateWindow("Penor",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,SCREENWIDTH,SCREENHEIGHT,SDL_WINDOW_OPENGL|SDL_WINDOW_SHOWN);
        if(window == NULL)
        {
            printf("Could not create window: %s\n",SDL_GetError());
            return 1;
        }
        else
        {
            if(InitGL()>0)
            {
                return 1;
            }
            //renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
        }
    }
    return 0;
}

void ClearRender(float r, float g, float b)
{
    glClearColor(r,g,b,1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

void StartRender()
{
    callnum = 0;
    textnum = 0;
    ttfnum = 0;
}

int DrawCmp(const void* a, const void* b)
{
    const struct DrawCall* lhs = (const struct DrawCall*)a;
    const struct DrawCall* rhs = (const struct DrawCall*)b;
    return (rhs->z < lhs->z) - (lhs->z < rhs->z);
}
    //((float)w/SCREENWIDTH)*2.0f
    //((float)x/SCREENWIDTH)*2.0f - 1.0f

void PresentRender()
{
    int texttexs[MAXTEXT];
    unsigned char* textbmps[MAXTEXT];
    for(int i=0;i<textnum;i++)
    {
        struct TextCall tc = textcalls[i];

        char* textFormatted = malloc(4096);
        char* curChar1 = tc.text;
        char* curChar2 = textFormatted;

        int w = 0;
        int h = 1;
        while(*curChar1 != 0)
        {
            if(*curChar1 == '\n')
            {
                if(w>0)
                {
                    while(w<tc.w)
                    {
                        *curChar2 = ' ';
                        w++;
                        curChar2++;
                    }
                }
            }
            else
            {
                *curChar2 = *curChar1;
                curChar2++;
                if(++w >= tc.w)
                {
                    w = 0;
                    h++;
                }
            }
            curChar1++;
        }
        if(w == 0)
        {
            h--;
        }
        if(w>0)
        {
            while(w<tc.w)
            {
                *curChar2 = ' ';
                curChar2++;
                w++;
            }
        }
        curChar2++;
        *curChar2 = 0;
        unsigned char* textBitmap = malloc(256*tc.w*h);
        curChar2 = textFormatted;
        for(int y = 0; y < h*8; y+=8)
        {
            for(int x = 0; x < tc.w*8; x+=8)
            {
                unsigned char offset = *(unsigned char*)curChar2;
                unsigned char* charBitmap = tex_font_data+(offset*256);
                for(int chary = 0;chary < 8;chary++)
                {
                    unsigned char* bitmapPos = ((y+chary)*tc.w*8+x)*4+textBitmap;
                    for(int charpix = 0;charpix < 8;charpix++)
                    {
                        *bitmapPos = *charBitmap;
                        bitmapPos++;
                        charBitmap++;
                        *bitmapPos = *charBitmap;
                        bitmapPos++;
                        charBitmap++;
                        *bitmapPos = *charBitmap;
                        bitmapPos++;
                        charBitmap++;
                        *bitmapPos = *charBitmap;
                        bitmapPos++;
                        charBitmap++;
                    }
                }
                curChar2++;
            }
        }

        glGenTextures(1,&texttexs[i]);
        glBindTexture(GL_TEXTURE_2D,texttexs[i]);

        glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,tc.w*8,h*8,0,GL_RGBA,GL_UNSIGNED_BYTE,textBitmap);

        const float tw = tc.w*8.0f*tc.scale;
        const float th = h*8.0f*tc.scale;
        
        float tx = 0,ty = 0;
        switch(tc.xalign)
        {
            default:
            case 0:
                tx = (float)tc.x+tw/2.0f;
                break;
            case 1:
                tx = (float)tc.x;
                break;
            case 2:
                tx = (float)tc.x-tw/2.0f;
                break;
        }
        switch(tc.yalign)
        {
            default:
            case 0:
                ty = (float)tc.y+th/2.0f;
                break;
            case 1:
                ty = (float)tc.y;
                break;
            case 2:
                ty = (float)tc.y-th/2.0f;
                break;
        }


        struct DrawCall textdc;
        textdc.texturenumber = texttexs[i];
        textdc.x = tx;
        textdc.y = ty;
        textdc.z = tc.z;
        textdc.filter = GL_NEAREST;
        textdc.w = tw;
        textdc.h = th;
        textdc.theta = 0.0f;
        textdc.r = tc.r;
        textdc.g = tc.g;
        textdc.b = tc.b;
        textdc.a = tc.a;
        textdc.tc.x = 0.0f;
        textdc.tc.y = 0.0f;
        textdc.tc.w = 1.0f;
        textdc.tc.h = 1.0f;
        textdc.temptex = 1;

        DrawTex(&textdc);

        free(textFormatted);
        if(tc.freetext)
            free(tc.text);
        
        textbmps[i] = textBitmap;
    }


    
    int ttftexs[MAXTTF];
    for(int i=0;i<ttfnum;i++)
    {
        SDL_Surface* ttfsurf = TTF_RenderText_Blended(namfonts[ttfcalls[i].font].font,ttfcalls[i].text,(SDL_Color){255,255,255,255});
        
        if(!ttfsurf)
        {
            char penis[256];
            sprintf(penis,"%i,%s\n",ttfcalls[i].font,ttfcalls[i].text);
            DEBUGMSGBOX(penis);
        }

        glGenTextures(1,&ttftexs[i]);
        glBindTexture(GL_TEXTURE_2D,ttftexs[i]);

        float tw = ttfsurf->pitch/ttfsurf->format->BytesPerPixel;
        float th = ttfsurf->h;

        glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,tw,th,0,GL_RGBA,GL_UNSIGNED_BYTE,ttfsurf->pixels);

        tw *= ttfcalls[i].scale;
        th *= ttfcalls[i].scale;

        float tx = 0,ty = 0;
        switch(ttfcalls[i].xalign)
        {
            default:
            case 0:
                tx = (float)ttfcalls[i].x+tw/2.0f;
                break;
            case 1:
                tx = (float)ttfcalls[i].x;
                break;
            case 2:
                tx = (float)ttfcalls[i].x-tw/2.0f;
                break;
        }
        switch(ttfcalls[i].yalign)
        {
            default:
            case 0:
                ty = (float)ttfcalls[i].y+th/2.0f;
                break;
            case 1:
                ty = (float)ttfcalls[i].y;
                break;
            case 2:
                ty = (float)ttfcalls[i].y-th/2.0f;
                break;
        }

        struct DrawCall textdc;
        textdc.texturenumber = ttftexs[i];
        textdc.x = tx;
        textdc.y = ty;
        textdc.z = ttfcalls[i].z;
        textdc.filter = GL_NEAREST;
        textdc.w = tw;
        textdc.h = th;
        textdc.theta = 0.0f;
        textdc.r = ttfcalls[i].r;
        textdc.g = ttfcalls[i].g;
        textdc.b = ttfcalls[i].b;
        textdc.a = ttfcalls[i].a;
        textdc.tc.x = 0.0f;
        textdc.tc.y = 0.0f;
        textdc.tc.w = 1.0f;
        textdc.tc.h = 1.0f;
        textdc.temptex = 1;

        DrawTex(&textdc);

        if(ttfcalls[i].freetext)
            free(ttfcalls[i].text);
        
        SDL_FreeSurface(ttfsurf);
    }




    qsort(drawcalls,callnum,sizeof(struct DrawCall),DrawCmp);

    for(int i=0;i<callnum;i++)
    {
        //i don't think modelview works in immediate mode so yay

        struct DrawCall dc = drawcalls[i];
        const float x1 = -dc.w/2.0f,y1 = -dc.h/2.0f;
        const float x2 = -x1,y2 = y1;
        const float x3 = x2,y3 = -y1;
        const float x4 = x1,y4 = y3;

        dc.theta *= -1;

        const float presin = sinf(dc.theta);
        const float precos = cosf(dc.theta);

        const float fx1 = (x1*precos+y1*presin+dc.x),fy1 = (x1*-presin+y1*precos+dc.y);
        const float fx2 = (x2*precos+y2*presin+dc.x),fy2 = (x2*-presin+y2*precos+dc.y);
        const float fx3 = (x3*precos+y3*presin+dc.x),fy3 = (x3*-presin+y3*precos+dc.y);
        const float fx4 = (x4*precos+y4*presin+dc.x),fy4 = (x4*-presin+y4*precos+dc.y);

        if(!dc.temptex)
            glBindTexture(GL_TEXTURE_2D,loadedTextures[dc.texturenumber]);
        else
            glBindTexture(GL_TEXTURE_2D,dc.texturenumber);

        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,dc.filter);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,dc.filter);
        
        glColor4f(dc.r,dc.g,dc.b,dc.a);
        glBegin(GL_QUADS);
            glTexCoord2f(dc.tc.x,dc.tc.y);
            glVertex2f(fx1,fy1);
            glTexCoord2f(dc.tc.x+dc.tc.w,dc.tc.y);
            glVertex2f(fx2,fy2);
            glTexCoord2f(dc.tc.x+dc.tc.w,dc.tc.y+dc.tc.h);
            glVertex2f(fx3,fy3);
            glTexCoord2f(dc.tc.x,dc.tc.y+dc.tc.h);
            glVertex2f(fx4,fy4);
        glEnd();
    }

    for(int i=0;i<ttfnum;i++)
    {
        glDeleteTextures(1,&ttftexs[i]);
    }

    for(int i=0;i<textnum;i++)
    {
        glDeleteTextures(1,&texttexs[i]);
        free(textbmps[i]);
    }

    SDL_GL_SwapWindow(window);
}