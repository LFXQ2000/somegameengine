SOURCES := $(shell find . -name "*.c")

OPT = -Ofast

all:
	pypy3 ttfgen.py
	pypy3 texgen.py
	pypy3 soundgen.py
	mkdir -p build
	gcc -Iinclude $(SOURCES) $(OPT) -lm -std=c17 -lGL -lGLU -lGLEW $$(pkg-config sdl2 --cflags --libs) $$(pkg-config SDL2_mixer --cflags --libs) $$(pkg-config SDL2_ttf --cflags --libs) -o build/somegameengine